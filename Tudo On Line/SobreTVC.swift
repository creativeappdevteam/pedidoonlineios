//
//  RestaranteTableVC.swift
//  Tudo On Line
//
//  Created by Orlando Amorim on 06/10/15.
//  Copyright © 2015 com.douglasalexandre. All rights reserved.
//

import UIKit

class SobreTVC: UITableViewController {
    
    var sobre:String = String()
    var imagem:String = String()
    var arraySections:[Int : Int] = [Int : Int]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arraySections = [0:1]
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableHeaderView = nil

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return arraySections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arraySections[section]!
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("SobreCell", forIndexPath: indexPath) as! RestSobreTVCell
        
        
        cell.textView.text = "O Coco Bambu Teresina encontra-se situado no Bairro Jockey Club, região mais nobre da cidade, mais precisamente na Avenida Dom Severino, em uma área de quase 4000 m². É sem dúvida, um dos cartões postais de Teresina e destino constante de clientes habitués e turistas."
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
    
        
        return cell
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Sobre"
        }
        return ""
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel!.textColor = UIColor(red: 76/255, green: 107/255, blue: 148/255, alpha: 1.0)
        header.textLabel!.textAlignment = NSTextAlignment.Center
        
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        }
        return 45
    }
    
//    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    
    // MARK: - Animações
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        //Criando um objeto do tipo NSNOtitcationCenter
        
        if scrollView.contentOffset.y <= 160{
            let centroDeNotificacao: NSNotificationCenter = NSNotificationCenter.defaultCenter()
            
            //ENVIANDO os dados por Object
            centroDeNotificacao.postNotificationName("moveuTableViewSobre", object: scrollView)
        }
        

    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    


}
